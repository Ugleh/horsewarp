package com.ugleh.vehiclewarp;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class VWListener implements Listener {

	@EventHandler(priority = EventPriority.HIGHEST)
	public void PlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
		if (event.getPlayer().hasPermission("horsewarp.use")) {
			String[] commandParams = event.getMessage().split(" ");
			String command = commandParams[0].replace("/", ""); // The command

			if (command.equals("warp")) {
				Entity thePlayer = event.getPlayer();
				final Player player = event.getPlayer();
				if (thePlayer.getVehicle() != null) {
					if (thePlayer.getVehicle().getType().getName()
							.equals(EntityType.HORSE)) {
						final Entity Horse = thePlayer.getVehicle();
						thePlayer.getVehicle().eject();
						Bukkit.getServer()
								.getScheduler()
								.scheduleSyncDelayedTask(VehicleWarp.main,
										new Runnable() {

											public void run() {
												Player p = Bukkit
														.getPlayer(player
																.getName());
												Horse.teleport(p);
												Horse.setPassenger(p);
											}
										}, 1L);// 60 L == 3 sec, 20 ticks == 1
												// sec
					}
				}
			}
		}
	}
}
